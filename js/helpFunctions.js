/**
 *	Set html element to dead center of screen.
 *	@param {HTML} element - The element that shall be set to position
 *	@param {boolean} element - x position
 *	@param {boolean} element - y position
 *	@return void.
 */
function centerElement(elem, x, y )
{
	if(x && y)
	{
		elem.css({
			'top' : $(window).height() / 2 - elem.outerHeight(true) / 2,
			'left' : $(window).width() / 2 - elem.outerWidth(true) / 2
		});
	}
	else if (x && !y)
	  elem.css('left' , $(window).width() / 2 - elem.outerWidth(true) / 2);
	else if (!x && y)
	  elem.css('top' , $(window).height() / 2 - elem.outerHeight(true) / 2);
}

/**
 * Random int from interval
 * @param int - min
 * @param int - max
 * @return int - random number from min to max.
 */
function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

/**
 * Validate string
 */
function validateString(str)
{
	// Accepts: a-å, A-Å and 0-9:
    return ( /^[a-zA-Z0-9~æøåÆØÅ]+$/.test(str));
}
