/**
 * Read numbers from server
 */
function readBalls() 		// Called from listener.js
{
	$.ajax
	({
		type: "GET",
		url: "CRUD/read/balls.php"
	})
	.done(function(data)
  {
		balls = JSON.parse(data);
	});
}

function readScores() 		// Called from balls.js
{
	$.ajax
	({
		type: "GET",
		url: "CRUD/read/scores.php"
	})
	.done(function(data)
  {
			globalScore = JSON.parse(data);
			console.log(globalScore);
	});
}

function readFlags()
{
  $.ajax
	({
		type: "GET",
		url: "CRUD/read/flags.php"
	})
  .done(function(data)
  {
    flags = JSON.parse(data);
  });
}
