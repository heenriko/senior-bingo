function updateFlags(dataFlags)
{
  $.ajax
  ({
    type: "POST",
    url: "CRUD/update/flags.php",
    data: {data : dataFlags}
  })
  .done(function(data)
  {
  //  console.log("flags updated");
  });
}

function initScores(names)
{
  var initPlayers = [];
  for(var i = 0; i < names.length; i++)
  {
    initPlayers.push({ name: names[i], score: 0 });
  }
   initPlayers = JSON.stringify(initPlayers);
  $.ajax
  ({
    type: "POST",
    url: "CRUD/update/initScores.php",
    data: {data : initPlayers}
  })
  .done(function(data)
  {
  console.log(data);
  });
}
