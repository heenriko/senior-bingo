"use strict";

/*----------------------------------------------------------------------------*/
/* 										V A R I A B L E S   D E C L R A T I O N
/*----------------------------------------------------------------------------*/

$(function()
{

});

var tempBall = "";
var base = 15;
var counter = 0;
var ballsIntervalTime = 10000;						// Interval per ball.
var ballsInterval;
/*----------------------------------------------------------------------------*/
/* 														F U N C T I O N S
/*----------------------------------------------------------------------------*/

/**
 * Generate balls and upload to server.
 */
function genBalls()
{
	for(var i = 0; i < NUM_OF_BALLS; i++)
	{
		if(i == 1 || i == 4 || i == 7 || i == 12)
		{
			switch(i)
			{
				case 1: tempBall = 'N30'; break;
				case 4: tempBall = 'N35'; break;
				case 7: tempBall = 'N40'; break;
				case 12: tempBall = 'N39'; break;
			}
		}
		else
		{
			do 																					// Avoid dupes:
			{
				var rl = Math.floor((Math.random() * 5));	// Random num: 0 - 4.
																									// Rand. letter + rand. number:
				tempBall = letters[rl] +
				randomIntFromInterval(base * rl + 1, base * (rl+1));

				tempBall.toString();
			}
			while( balls.indexOf(tempBall) != -1);		  // This ball exists.
		}


		balls[i] = tempBall;												// Assign ball.
	}

	$.ajax
  ({
    type: "POST",
    url: "CRUD/update/balls.php",
    data: {balls : balls.join(',')}
  })
  .done(function(data)
  {
  	//console.log(balls);
  });
}

/**
 *  Display balls iteration
 */
function displayBalls()
{
	if(counter == 0)
	{
		displayNextBall(counter);
		counter++;
	}

	ballsInterval = setInterval(function()
	{
		if (counter < balls.length)
		{
			displayNextBall(counter);
		}
		else // Last ball is drawn, Game ends:
		{
			console.log('sdf')
			clearInterval(ballsInterval);
			$('.ballsContainer .ball').css('display','none');

			$('.endGameMessage').css('display','block');


		}

		counter++;
	},ballsIntervalTime);
}

/**
 * Render balls with animation
 */
function displayNextBall(index)
{
	var ball = $('.ballsContainer .ball');
	var char = balls[index].charAt(0);
	var style = "";
	var style1 = "";
	var style2 = "";
	var style3 = "";

	switch(char)
	{
		case "B": style = 'background:' + letterColors[0] + ';'; break;
		case "I": style = 'background:' + letterColors[1] + ';'; break;
		case "N": style = 'background:' + letterColors[2] + ';'; break;
		case "G": style = 'background:' + letterColors[3] + '; color:#000;'; break;
		case "O": style = 'background:' + letterColors[4] + ';'; break;
	}

	ball.text(balls[index]);
	ball.attr('class','ball animated fadeInRight')
	.attr('style',style);

	if(index == 1 )
	{
		var char1 = balls[index-1].charAt(0);

		switch(char1)
		{
			case "B": style1 = 'background:' + letterColors[0] + ';'; break;
			case "I": style1 = 'background:' + letterColors[1] + ';'; break;
			case "N": style1 = 'background:' + letterColors[2] + ';'; break;
			case "G": style1 = 'background:' + letterColors[3] + '; color:#000;'; break;
			case "O": style1 = 'background:' + letterColors[4] + ';'; break;
		}

		$('.smallBalls li').eq(0).text(balls[index-1]).attr('style',style1);
	}
	else if(index == 2 )
	{
		var char1 = balls[index-1].charAt(0);
		var char2 = balls[index-2].charAt(0);

		switch(char1)
		{
			case "B": style1 = 'background:' + letterColors[0] + ';'; break;
			case "I": style1 = 'background:' + letterColors[1] + ';'; break;
			case "N": style1 = 'background:' + letterColors[2] + ';'; break;
			case "G": style1 = 'background:' + letterColors[3] + '; color:#000;'; break;
			case "O": style1 = 'background:' + letterColors[4] + ';'; break;
		}

		switch(char2)
		{
			case "B": style2 = 'background:' + letterColors[0] + ';'; break;
			case "I": style2 = 'background:' + letterColors[1] + ';'; break;
			case "N": style2 = 'background:' + letterColors[2] + ';'; break;
			case "G": style2 = 'background:' + letterColors[3] + '; color:#000;'; break;
			case "O": style2 = 'background:' + letterColors[4] + ';'; break;
		}

		$('.smallBalls li').eq(0).text(balls[index-1]).attr('style',style1);
		$('.smallBalls li').eq(1).text(balls[index-2]).attr('style',style2);
	}
	else if(index > 2 )
	{
		var char1 = balls[index-1].charAt(0);
		var char2 = balls[index-2].charAt(0);
		var char3 = balls[index-3].charAt(0);

		switch(char1)
		{
			case "B": style1 = 'background:' + letterColors[0] + ';'; break;
			case "I": style1 = 'background:' + letterColors[1] + ';'; break;
			case "N": style1 = 'background:' + letterColors[2] + ';'; break;
			case "G": style1 = 'background:' + letterColors[3] + '; color:#000;'; break;
			case "O": style1 = 'background:' + letterColors[4] + ';'; break;
		}

		switch(char2)
		{
			case "B": style2 = 'background:' + letterColors[0] + ';'; break;
			case "I": style2 = 'background:' + letterColors[1] + ';'; break;
			case "N": style2 = 'background:' + letterColors[2] + ';'; break;
			case "G": style2 = 'background:' + letterColors[3] + '; color:#000;'; break;
			case "O": style2 = 'background:' + letterColors[4] + ';'; break;
		}

		switch(char3)
		{
			case "B": style3 = 'background:' + letterColors[0] + ';'; break;
			case "I": style3 = 'background:' + letterColors[1] + ';'; break;
			case "N": style3 = 'background:' + letterColors[2] + ';'; break;
			case "G": style3 = 'background:' + letterColors[3] + '; color:#000;'; break;
			case "O": style3 = 'background:' + letterColors[4] + ';'; break;
		}

		$('.smallBalls li').eq(0).text(balls[index-1]).attr('style',style1);
		$('.smallBalls li').eq(1).text(balls[index-2]).attr('style',style2);
		$('.smallBalls li').eq(2).text(balls[index-3]).attr('style',style3);
	}

	setTimeout(function()
	{
		if(!stopBalls)
			ball.attr('class','ball animated fadeOutLeft');
		else {
			ball.attr('class','ball').css('opacity',1);
		}
	},ballsIntervalTime-500);
}

function displayScore()
{
	var readScoreInterval = setInterval(function()
	{
		readScores();

		if(globalScore.length == names.length)
		{
			$('.ballsContainer').css('display','none');
			$('.scoreSection').css('display','block');

			clearInterval(readScoreInterval);
			console.log('done');
			// Sort the scores:
			globalScore.sort(function(a, b) {
				return - ( (a.score - b.score) || a.name.localeCompare(b.name) );
			});

			var items = "";
			var animation = "";

			for(var i = 0; i <globalScore.length; i++)
			{
				if(i == 0)
					animation = "animated rubberBand";
				else {
					animation = "animated fadeInUp";
				}
				items += "<li class = '" + animation + "'>" + (i+1) + ". " + globalScore[i].name
				+ " - " + globalScore[i].score + " poeng. </li>";
			}


			$('.playerScore').append(items);
		}

	},1000);




}

genBalls();                          // Generate the "balls".
