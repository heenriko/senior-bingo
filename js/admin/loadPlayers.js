"use strict";

/*----------------------------------------------------------------------------*/

	// Start:
	$(function()
	{
		 loadPlayersNames();
	});

/*----------------------------------------------------------------------------*/
/* 										V A R I A B L E S   D E C L R A T I O N
/*----------------------------------------------------------------------------*/

var curNumPlayers = 0;

/*----------------------------------------------------------------------------*/
/* 														F U N C T I O N S
/*----------------------------------------------------------------------------*/

/**
 * Listen after players that joins the game.
 */
function loadPlayersNames()
{
	$.ajax
	({
		type: "GET",
		url: "CRUD/read/playerNames.php"
	}).done(function(data)
  {
		if(names[0] != null )
			curNumPlayers = names.length;

  	names = JSON.parse(data);

		if(names[0] != null )
			printPlayerNames(names, curNumPlayers);
		else
			clearList();

		// Loop:
		if (flags.searchForPlayers == true || flags.searchForPlayers == "true")
		{
			setTimeout( function()
			{
				console.log('listening');
				loadPlayersNames();

			}, 1000);
		}
		else {
			console.log("Stopped listening for players");
		}
  });
}

function clearList(data)
{
	$('.players').empty();
}

function printPlayerNames(data, startPos)
{
	var li = "";
	for(var i = startPos; i < data.length; i++)
	{
		li += "<li class = 'animated zoomIn'>" + data[i] + "</li>";
	}

	$('.players').append(li);
}
