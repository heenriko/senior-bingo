/*
	AUTHOR: 	Henrik Oddløkken

							T A B L E   O F   C O N T E N T S



*/

"use strict";

/*----------------------------------------------------------------------------*/

	// Start:
	$(function()
	{

	});

/*----------------------------------------------------------------------------*/
/* 										V A R I A B L E S   D E C L R A T I O N
/*----------------------------------------------------------------------------*/



var intervalFlags;
var fakebingo = true;

// CONSTANTS
var MIN_NUM_PLAYERS = 1;
var MAX_NUM_PLAYERS = 20;
var NUM_OF_BALLS = 15;

// Flags:
var flags =
{
	searchForPlayers: true, 				// Stays true until the game starts.
	gameStarted : false
};
updateFlags(flags);

// Arrays:
var names = [];
var letters = ['B','I','N','G','O'];
var balls = [];
var globalScore = [];

var letterColors =
[
	'hsl(0,75%,50%)',
	'hsl(200,75%,50%)',
	'hsl(125,75%,50%)',
	'hsl(60,75%,50%)',
	'hsl(300,75%,50%)'
];

// var ticketInnerColorsNumber =
// [
// 	'hsl(0,75%,20%)',
// 	'hsl(200,75%,20%)',
// 	'hsl(150,75%,20%)',
// 	'hsl(65,75%,20%)',
// 	'hsl(300,75%,20%)'
// ];

// Booleans
var stopBalls = false;

intervalFlags = setInterval( function()
{
	readFlags();
	if (flags.searchForPlayers == true || flags.searchForPlayers == "true")
	{
		clearInterval(intervalFlags);
	}
}, 1000);
