$(document).ready(function()
{

/*----------------------------------------------------------------------------*/
/* 												        A D M I N
/*----------------------------------------------------------------------------*/

/**
 * Admin Starts the game:
 */
$('#startGame').on('click',function()
{
  // Load names:
  $.ajax
	({
		type: "GET",
		url: "CRUD/read/playerNames.php"
	}).done(function(data)
  {
    names = JSON.parse(data);
    if(names.length >= MIN_NUM_PLAYERS)
    {
      flags.searchForPlayers = false;      // Stop listening for more players.
      flags.gameStarted = true;            // Game starts.
      updateFlags(flags);                  // Update flags.

      /* Switch to ball screen */
      $('.loadingPlayers').css('display','none');
      $('.gameScreen').css('display','block');
      displayBalls();
      initScores(names);
    }
    else {
      alert('Need more players to play');
    }

  });
});

$('#stopBall').on('click',function()
{
  var ball = $('.ballsContainer .ball');
	console.log('stopped');
	ball.attr('class','ball').css('opacity',1);
  clearInterval(ballsInterval);
  $('#stopBall').hide();
  stopBalls = true;
  $('.bingoStatusMsgAdmin[name="pause"]').css('display','block');
});

// resume game from pause:
$('.bingoStatusMsgAdmin[name="pause"] button').on('click',function()
{
  $('#stopBall').show();
  console.log('pause');
  $('.bingoStatusMsgAdmin[name="pause"]').css('display','none');
  displayBalls();
  stopBalls = false;
});

// resume game from bingo msg:
$('.bingoStatusMsgAdmin[name="bingo"] button').on('click',function()
{
  $('#stopBall').show();
  $('.bingoStatusMsgAdmin[name="bingo"]').css('display','none');
  displayBalls();
  stopBalls = false;
});

$('#endGameButton').on('click',function()
{
  displayScore();
});

// Wizard of OZ
// Middle mouse button - show bingo for player 1:
$(document).mousedown(function(e)
{
	if (e.which == 2)
	{
    e.preventDefault();
    $('#stopBall').hide();
    var ball = $('.ballsContainer .ball');
  	console.log('stopped');
  	ball.attr('class','ball').css('opacity',1);
    clearInterval(ballsInterval);

    var str = names[0].toLowerCase();

    stopBalls = true;


    $('.bingoStatusMsgAdmin[name="bingo"]').css('display','block');
    $('.bingoStatusMsgAdmin[name="bingo"]').find('.bigText').html(str + " har<br>bingo")
  }
});




/*----------------------------------------------------------------------------*/
/* 												       P L A Y E R
/*----------------------------------------------------------------------------*/

/**
 * Player enters name and joins the game:
 */
$('#joinGameSubmit').on('click', function()
{
  joinGameForm();
});

/**
 * Playing Bingo:
 * Click on ticket
 */

// Button check / uncheck

$('body').delegate('.ticket .num','click',function()
{
  var elem = $(this);
  var data = elem.data('slot');

  var letNum = letters[data[0]] + elem.text();

  if( !elem.hasClass('greenTile') )               // Element is not bingoed!
  {
    if(!elem.hasClass('checked'))                 // Element is not checked:
    {
      elem.addClass('checked');
      ticket[data[0]][data[1]].cssChecked = true;

    	if(balls.indexOf(letNum) != -1)					 // ball exists:
        ticket[data[0]][data[1]].checked = true;

    }
    else                                         // Element is checked:
    {
      elem.removeClass('checked');
      ticket[data[0]][data[1]].checked = false;
      ticket[data[0]][data[1]].cssChecked = false;
    }
  }

  if(ticket[2][0].checked && ticket[2][1].checked && ticket[2][3].checked && ticket[2][4].checked && fakebingo)
  {
    fakebingo = false;
    $('#bingoButton').addClass('bingoButtonActive');
  }

});

$('#bingoButton').on('click',function()
{
  // insert Stop animation
  validateTicket();
  setScore();
  player.upload();
  $(this).removeClass('bingoButtonActive');
});

$(document).keyup(function(event){
    if(event.keyCode == 13 && $('.joinGame input').is(":focus"))
    {
        console.log('enter');
        joinGameForm();
    }
});


/*------*/

function joinGameForm()
{
  var name = $('.joinGame input').val().toString();     // Get input field value.

  // Load names:
  $.ajax
	({
		type: "GET",
		url: "CRUD/read/playerNames.php"
	}).done(function(data)
  {
    names = JSON.parse(data);

    if (flags.searchForPlayers == true || flags.searchForPlayers == "true")
    {
      if (validateString(name) )            // String is valid:
      {
        if (names.indexOf(name) == -1)      // Name is not taken:
        {
          $.post( "CRUD/update/playerNames.php",
          { name: name}).done(function(data)
          {
            console.log( "Data Loaded: " + data );
            player.name = name;

            $('.joinGameSection').css('display','none');
            $('.signedIn').css('display','block');
            animateLogin();
          });
        }
        else {
          alert(name + " is already taken");
        }
      }else {
        alert('invalid username');
      }
    }else {
      alert('Session timeout');
    }
  });
}


/*----------------------------------------------------------------------------*/
});// End doc ready.
