/*
	AUTHOR: 	Henrik Oddløkken

							T A B L E   O F   C O N T E N T S



*/

/*----------------------------------------------------------------------------*/


	$(function()
	{
		generateTicket();


		bingoScore =
		({
			'column':[],
			'row':[],
			'diagonal':[]
		});
	});

/*----------------------------------------------------------------------------*/
/* 										V A R I A B L E S   D E C L R A T I O N
/*----------------------------------------------------------------------------*/

var tickets = [];
var ticket = [];												// Ticket array
var bingoScore = [];

var rows = 5;
var cols = 5;

var scorePoints = 5;



/*----------------------------------------------------------------------------*/
/* 						 						W I N D O W   F U N C T I O N S
/*----------------------------------------------------------------------------*/

	$(window).resize(function()
	{
		centerElement ( $('.ticket'), true, true);
	});


/*----------------------------------------------------------------------------*/
/* 														F U N C T I O N S
/*----------------------------------------------------------------------------*/

/**
 * Generate ticket
 *
 */
function generateTicket()
{
	var value = 0; 																	// Declare random variable.
	var checked = false;
	var base = 15;
	var usedNums = [];

	for (var i = 0; i < rows; i++) 									// Rows:
	{
		ticket[i] = [];																// 2D array.

		for (var j = 0; j < cols; j++)								// Columns:
		{
			checked = false;														// Reset.

			if (i == 2)
			{
				switch(j)
				{
					case 0: value = 30; break;
					case 1: value = 35; break;
					case 2: value = "&#9733;"; checked = true; break;
					case 3: value = 40; break;
					case 4: value = 39; break;
				}
			}
			else
			{
				do {																			// Avoid duplicates:
					value = randomIntFromInterval						// Generate a random number e.g (1 - 15).
					( base * i + 1, base * (i+1) );
				}
				while( usedNums.indexOf(value) != -1);		// Continues if the number
																										// doesn't exists.
			}
			usedNums.push(value);
			ticket[i][j] = {'val': value, 'checked': checked, 'cssChecked': checked };	// Assign random number.
		}
	}
}

/**
 * Render Ticket in HTML
 *
 */
function renderTicket(greenTiles)
{
	var items = "";
	var classes = "";
	var style = "";

	for (var i = 0; i < ticket.length; i++)
	{
		// Set letter background color:
		style = "style='background:" + letterColors[i] + ";'";

		items += "<li class = 'letter'" + style + ">"+ letters[i] +"</li>";

		for (var j = 0; j < ticket[i].length; j++)
		{
			//style = "style='background:" + ticketInnerColorsNumber[i] + ";'";
			style = "style=''";

			if(greenTiles != null)
			{
				classes = "";

				if(ticket[i][j].cssChecked || ticket[i][j].val == null)
				{
					classes += " checked ";

					if( greenTiles.indexOf( i + "" + j ) != -1 )
						classes = "greenTile animated bounce";
				}


				if (j == 2 && i == 2)
					items += "<li class='" + classes + "' " + style + " data-slot='[" + i + "," + j + "]'>" + ticket[i][j].val + "</li>";
				else
				{
					classes += " num"
					items += "<li class='" + classes + "' " + style + " data-slot='[" + i + "," + j + "]'>" + ticket[i][j].val +"</li>";
				}
			}
			else {
				if (j == 2 && i == 2)
					items += "<li class='checked' " + style + " data-slot='[" + i + "," + j + "]'>" + ticket[i][j].val + "</li>";
				else
					items += "<li class = 'num' "+ style + " data-slot='[" + i + "," + j + "]'>"+ ticket[i][j].val +"</li>";
			}
		}
	}

	$('.ticket').append(items);
	centerElement ( $('.ticket'), true, true);
}

/**
 * Validate Bingo
 * 	Updates the score
 */
function validateTicket()
{
	var checkBingo = false;

	var greenTiles = [];

	// Validate the ticket for bingo:

	for(var i = 0; i < 5; i++)
	{
			// Columns:
			if( ticket[i][0].checked && ticket[i][1].checked && ticket[i][2].checked
					&& ticket[i][3].checked && ticket[i][4].checked )
			{
				console.log('Bingo at col ' + i );
				bingoScore.column[i] = true;
				checkBingo = true;

				greenTiles.push( i+"0", i+"1", i+"2", i+"3", i+"4" );
			}

			// Rows:
			if( ticket[0][i].checked && ticket[1][i].checked && ticket[2][i].checked
					&& ticket[3][i].checked && ticket[4][i].checked )
			{
				console.log('Bingo at row ' + i );
				bingoScore.row[i] = true;
				checkBingo = true;
				greenTiles.push( "0"+i, "1"+ i, "2"+i, "3"+i, "4"+i );

			}
	}

	// Diagonal
	if( ticket[0][0].checked && ticket[1][1].checked && ticket[2][2].checked &&
			ticket[3][3].checked && ticket[4][4].checked )
	{
		bingoScore.diagonal[0] = true;
		console.log('Bingo at diagonal bottom right');
		checkBingo = true;
		greenTiles.push( "0"+"0", "1"+"1", "2"+"2", "3"+"3", "4"+"4" );
	}

	if( ticket[4][0].checked && ticket[3][1].checked && ticket[2][2].checked &&
			ticket[1][3].checked && ticket[0][4].checked )
	{
		bingoScore.diagonal[1] = true;
		console.log('Bingo at diagonal bottom left');
		checkBingo = true;
		greenTiles.push( "4"+"0", "3"+"1", "2"+"2", "1"+"3", "0"+"4" );
	}



	// Display message:
	if(!checkBingo)
	{
		$('.bingoStatusMsg').css('display','block')
		$('.bigText').html('beklager<br>du har ikke<br>bingo');
		$('.smallText').html('markert feil på brett');

	}
	else {
		$('.bingoStatusMsg').css('display','block')
		$('.bigText').html('du har<br>bingo');
		$('.smallText').html('+5 poeng');

		// Render Ticket
		$('.ticket').empty();		// Reset.
		renderTicket(greenTiles);

	}
	setTimeout (function()
	{
		$('.bingoStatusMsg').fadeOut();
	},3000);


}

function setScore()
{
	var score = 0;

	for(var i = 0; i < 5; i++)
	{
		if(bingoScore.column[i])
			score += scorePoints;

		if(bingoScore.row[i])
			score += scorePoints;
	}

	if(bingoScore.diagonal[0])
		score += scorePoints;

	if(bingoScore.diagonal[1])
		score += scorePoints;


	player.score = score;
	console.log(player.score);
}
