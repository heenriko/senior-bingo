var counter = 0;

function animateLogin()
{
  if(counter < 5)
  {
    setTimeout(function()
    {
      $('.signedIn .bingoBalls li').eq(counter).css('display','inline-block');
      $('.signedIn .progressBar .bar .innerBar').animate
      ({
          'width' :  (counter +1) * 20 + "%"
      });
      $('.signedIn .progressBar .label').text((counter +1) * 20 + "%")
      counter++;
      animateLogin();
    },700);

  }
  if(counter == 4)
  {
    setTimeout(function()
    {
        $('.loginSuccessMsg').fadeIn();
    },700);

    setTimeout(function()
    {
      $('.signedIn').css('display','none');
    },2500);

    setTimeout(function()
    {

      $('.theGameSection').css('display','block');
      renderTicket(null);
    },3000);
  }

}
