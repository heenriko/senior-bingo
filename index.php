<?php include "CRUD/create/initFiles.php"; ?>
<?php include("include/head.php") ?>

<!-- Style -->
<link rel="stylesheet" href="css/admin/displayBalls.css"/>
<link rel="stylesheet" href="css/admin/score.css"/>
<link rel="stylesheet" href="css/mediaQuery.css"/>
<!-- Javascript -->
<script type = "text/javascript" src = "js/admin/balls.js"></script>
<script type = "text/javascript" src = "js/admin/loadPlayers.js"></script>
<script type = "text/javascript" src = "js/userInteraction.js"></script>

</head>
<body>
	<div class = "loadingPlayers">
		<div class ="grayBox">
			<h3>spillere</h3>
			<ul class = "players"></ul>
		</div>

		<button class = "purpleButton" id = "startGame">Start Game</button>
	</div>
	<div class="gameScreen">
		<div class="bingoStatusMsgAdmin" name="pause">
			<div class="bigText">spill pauset</div>
			<button class = "purpleButton" style = "margin-top:150px;">fortsett spill</button>
		</div>

		<div class="bingoStatusMsgAdmin" name="bingo">
			<div class="bigText"></div>
			<button class = "purpleButton" style = "margin-top:100px;">fortsett spill</button>
		</div>

		<div class = "ballsContainer">
			<div class="ball animated"></div>
			<ul class = "smallBalls" reversed>
				<li></li>
				<li></li>
				<li></li>
			</ul>

			<button type="button" id="stopBall" class = "whiteButton">Pause</button>

			<div class="endGameMessage">
				<div class="bingoStatusMsgAdmin" style = "display:block" name="bingo">
					<div class="bigText">bingo ferdig</div>
					<button class = "purpleButton" style = "margin-top:100px; font-size: 60%;" id = "endGameButton">gå til resultat</button>
				</div>
			</div>
		</div>
		<div class="scoreSection">
			<h3 class = "title">RESULTATER</h3>
			<ul class = "playerScore"></ul>
		</div>
	</div>

</body>
</html>
