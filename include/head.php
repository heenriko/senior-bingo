<!doctype html>
<html lang= "en">
<head>
	<meta name="viewport" content="width = device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<!-- Chrome, Firefox OS and Opera -->
	<meta name="theme-color" content="#00091a">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#00091a">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#00091a">

	<title>Bingo</title>

	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:300' rel='stylesheet' type='text/css'/>

	<!-- Style -->
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/main.css"/>

	<!-- Javascript -->
	<script type = "text/javascript" src = "js/update.js"></script>
	<script type = "text/javascript" src = "js/read.js"></script>
	<script type = "text/javascript" src = "js/helpFunctions.js"></script>
	<script type = "text/javascript" src = "js/main.js"></script>
