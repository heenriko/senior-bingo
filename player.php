<?php include("include/head.php") ?>

	<!-- Style -->
<link rel="stylesheet" href="css/player/joinGameForm.css"/>
<link rel="stylesheet" href="css/player/ticket.css"/>
<link rel="stylesheet" href="css/player/signedIn.css"/>
<link rel="stylesheet" href="css/player/gameElements.css"/>
<link rel="stylesheet" href="css/mediaQuery.css"/>

<!-- Javascript -->
<script type = "text/javascript" src = "js/player/Player.js"></script>
<script type = "text/javascript" src = "js/player/signedIn.js"></script>
<script type = "text/javascript" src = "js/player/ticket.js"></script>
<script type = "text/javascript" src = "js/player/listener.js"></script>
<script type = "text/javascript" src = "js/userInteraction.js"></script>


</head>
<body>
	<div class="joinGameSection">
		<h2 class = "mainLogo animated flipInX"><span style = "font-size:60%; letter-spacing: 20px;">senior</span><br>bingo</h2>
		<div class = "welcomeMsg">Vennligst skriv inn ditt navn</div>
		<div class="joinGame">
			<input type="text" name="name" value="" placeholder="Navn" autocomplete="off">
			<button id="joinGameSubmit" type="submit" name="button">Start Spill</button>
		</div>
	</div>
	<div class="signedIn">
		<ul class = "bingoBalls">
			<li class="animated fadeInRight" style = "background:hsl(0,50%,50%);">B</li>
			<li class="animated fadeInRight" style = "background:hsl(200,50%,50%);">I</li>
			<li class="animated fadeInRight" style = "background:hsl(150,50%,50%);">N</li>
			<li class="animated fadeInRight" style = "background:hsl(50,50%,50%); color: #000;">G</li>
			<li class="animated fadeInRight" style = "background:hsl(300,50%,50%);">O</li>
		</ul>
		<div class="progressBar">
			<div class="bar">
					<div class="innerBar"></div>
			</div>
			<div class="label">0%</div>
		</div>
		<div class = "loginSuccessMsg">Du er nå logget inn</div>
	</div>

	<div class="theGameSection">
		<button id = "bingoButton">bingo</button>
		<div class="bingoStatusMsg animated fadeIn">
			<div class="bigText"></div>
			<div class="smallText"></div>
		</div>
		<ul class='ticket'></ul>
	</div>

</body>
</html>
