<?php
// Run from JS:

$players = array();

foreach (glob("../../data/players/*.*") as $filename)
{
    $file = fopen("../../data/players/$filename", "r") or die("Unable to open file!");
    $data = json_decode(fgets($file));
    array_push($players, $data);
    fclose($file);
}

echo json_encode($players);
