<?php
// Run from JS:

$name = $_POST["name"];

if( validString($name) )
{
  $myfile = fopen("../../data/names.csv", "a");
  clearstatcache();
  if(filesize("../../data/names.csv"))        // File is not empty:
    $txt = "," . $name;
  else
    $txt = $name;

  fwrite($myfile, $txt);
  fclose($myfile);
  echo $name;
}
else {
  echo "String not valid";
}

/*
	This function checks if the user typed in any invalid characters.
	If the user types a invalid character, the function returns false.
*/

function validString($input)						// Function xss: parameter: the content of input field.
{
	$valid = true;							// Boolean variable to check if character is valid

											// Array of invalid characters:
	$char = array('"', '#', '&', '(',
					')', '/', ';', '<', '>');

	for($i = 0; $i < count($char); $i++ )	// Loop through all invalid characters:
	{
		$result = strpos($input, $char[$i]);	// Check if input has this character.
		if($result !== false) 					// Found character.
		{
			$valid = false;						// Set bool value to false.
			break;								// It's not necessary to loop further.
		}
	}

	if($valid) 									// There are no invalid characters in the input var:
		return true;
	else										// There is a invalid character in the input var:
		return false;
}
