<?php
// Run from JS:


$data = json_decode($_POST["data"]);          // Player names and scores

foreach ($data as $obj) {
  $name = $obj->name;
  $file = fopen("../../data/players/$name.json", "w");
  fwrite($file, '{"name":"' . $obj->name . '","score":"' . $obj->score .'"}');
  fclose($file);
}
