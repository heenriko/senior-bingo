# README #

### How do I start? ###

The application has currently been tested only with Google Chrome and Android devices (and windows 10 computer)

The application is available from these two pages, 
please open in this order:

    1. henriko.no/bingo/index.php
    2. henriko.no/bingo/player.php

In the first link, the application waits for players to join the game.

In the second link, the players can join the game (you can open as many 
tabs you want for this page to have more players).